import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CartOverviewComponent } from './screens/cart-overview/cart-overview.component';
import { ProductsTableComponent } from './components/product/products-table/products-table.component';
import { ProductInfoComponent } from './components/product/product-info/product-info.component';
import { QuantityCounterComponent } from './components/base/quantity-counter/quantity-counter.component';
import { ButtonComponent } from './components/base/button/button.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent, CartOverviewComponent, ProductsTableComponent, ProductInfoComponent, QuantityCounterComponent, ButtonComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
