import { Component, OnInit } from '@angular/core';
import { CheckoutCartService } from 'src/app/services/checkout-cart.service';

import { applyDiscountThreeUnits, applyDiscountTwoPerOne, CartOverview, AvailableProducts, Discount, DiscountConfig, Product, ProductCode, ProductItem } from 'business';

const MOCK_DISCOUNTS_THREE_UNITS_CAP_TWO_PER_ONE_TSHIRT: DiscountConfig[] = [
  {
    name: 'x3 Cap offer',
    applyOver: 'CAP',
    discountFuntion: applyDiscountThreeUnits
  },
  {
    name: '2x1 TShirt Offer',
    applyOver: 'TSHIRT',
    discountFuntion: applyDiscountTwoPerOne
  }
];

const MOCK_AVAILABLE_PRODUCTS: AvailableProducts = new Map<ProductCode, ProductItem>([
  ['TSHIRT', new Product('TSHIRT', 'shirt.png', 'Cabify T-Shirt', 20)],
  ['MUG', new Product('MUG', 'mug.png', 'Cafify Coffee Mug', 5)],
  ['CAP', new Product('CAP', 'cap.png', 'Cabify Cap', 10)]
]);
@Component({
  selector: 'app-cart-overview',
  templateUrl: './cart-overview.component.html',
  providers: [CheckoutCartService, { provide: 'discountConfig', useValue: MOCK_DISCOUNTS_THREE_UNITS_CAP_TWO_PER_ONE_TSHIRT }, { provide: 'products', useValue: MOCK_AVAILABLE_PRODUCTS }]
})
export class CartOverviewComponent implements OnInit, CartOverview {
  public productsList: ProductItem[] = [];

  constructor(private checkoutCartService: CheckoutCartService) {}

  public ngOnInit(): void {
    this.scanBaseProducts();
  }

  private scanBaseProducts(): void {
    MOCK_AVAILABLE_PRODUCTS.forEach((product: ProductItem) => {
      this.checkoutCartService.scan(product.code);
    });
    this.updateComponentProducts();
  }

  public updateComponentProducts(): void {
    this.productsList = this.checkoutCartService.getCart();
  }

  public onCheckout(): void {
    this.checkoutCartService.emptyCart();

    this.scanBaseProducts();
  }

  public onChangeProductQuantity({ newQuantity, productCode }: { newQuantity: number; productCode: ProductCode }): void {
    this.checkoutCartService.changeQuantity(productCode, newQuantity);

    this.updateComponentProducts();
  }

  public get totalAmountOfProducts(): number {
    return this.checkoutCartService.getTotalAmountOfProducts();
  }

  public get totalBasePriceOfProducts(): number {
    return this.checkoutCartService.getTotalBasePriceOfProducts();
  }

  public get discounts(): Discount[] {
    return this.checkoutCartService.getDiscounts();
  }

  public get totalPriceWithDiscount(): number {
    return this.checkoutCartService.total();
  }
}
