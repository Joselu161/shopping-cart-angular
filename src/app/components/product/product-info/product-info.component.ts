import { Component, Input } from '@angular/core';
import { ProductDetail, ProductInfo } from 'business';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html'
})
export class ProductInfoComponent implements ProductInfo {
  @Input() productInfo!: ProductDetail;
}
