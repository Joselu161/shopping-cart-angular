import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { ProductCode, ProductItem, ProductsTable } from 'business';

@Component({
  selector: 'app-products-table',
  templateUrl: './products-table.component.html'
})
export class ProductsTableComponent implements ProductsTable {
  @Input() public products: ProductItem[] = [];
  @Output() public changeProductQuantity = new EventEmitter<{ newQuantity: number; productCode: ProductCode }>();

  public trackBy(index: number, item: ProductItem) {
    return item.code;
  }
}
