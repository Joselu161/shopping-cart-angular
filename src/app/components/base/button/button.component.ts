import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Button } from 'business';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html'
})
export class ButtonComponent implements Button {
  @Input() public buttonText: string = 'Button';
  @Output() public clickEvent = new EventEmitter();
}
